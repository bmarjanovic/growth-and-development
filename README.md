# Growth and Development

This is a personal project to help me:

- Track my personal growth & development within GitLab.
- Track my personal achievements.
- Track my weekly work progress.
- Help me assess my strengths as well as my weaknesses and improvement opportunities.


Tracking Issues and Documents

- [Weekly Tracking](#1)
- [Mid-Year Check In](#2)
- [Self-Assessment](https://docs.google.com/document/d/1obZ5WL1WeTm4gYJoBNvgZdH2Aws8ecqqg9DVw6Qz8bU/edit?usp=drive_link)
- [Integrations Work Tracker](https://docs.google.com/document/d/17_O5mHa83qCeTnH06ZU8M6roo8TGs1dhCFqJUhIJ_T0/edit#heading=h.fw2x2soy7hag)
